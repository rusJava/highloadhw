## Prepare test data

#### 1. Run docker image
```docker run --name mysql_l8 -e MYSQL_ROOT_PASSWORD=root  --rm -p 3306:3306 -d mysql:8.0```  
or laradoc  
```./sync.sh up mysql```

#### 2. Create db and table
```
docker exec -it mysql_l8 mysql -uroot -proot 
create database l8_hl;

drop table if exists user;
create table user
(
    id         bigint       not null AUTO_INCREMENT,
    f_name     varchar(10)  not null,
    l_name     varchar(10)  not null,
    gender     varchar(1)   not null,
    birth_date datetime     not null,
    primary key (id)
);
```

#### 3. Create scripts

```
DROP procedure if exists generate_data;
DELIMITER $$
CREATE PROCEDURE generate_data(row_count int)
BEGIN
    DECLARE i INT DEFAULT 0;
    WHILE i < row_count
        DO
            INSERT INTO `user` (f_name, l_name, gender, birth_date)
            VALUES (get_string(10),
                    get_string(10),
                    get_gender(),
                    FROM_UNIXTIME(UNIX_TIMESTAMP('1980-01-01 01:00:00') + FLOOR(RAND() * 915360000)));
            SET i = i + 1;
        END WHILE;
END$$
DELIMITER ;

DROP function if exists get_gender;
CREATE FUNCTION get_gender() RETURNS VARCHAR(1) DETERMINISTIC
BEGIN
    DECLARE gender varchar(1);
    IF rand()*2 > 1 THEN
        SET gender = 'm';
    ELSE
        SET gender = 'w';
    END IF;
    RETURN (gender);
END;

DROP procedure if exists generate_data;
DELIMITER $$
CREATE PROCEDURE generate_data(row_count int)
BEGIN
    DECLARE i INT DEFAULT 0;
    WHILE i < row_count
        DO
            INSERT INTO `user` (f_name, l_name, gender, birth_date)
            VALUES (get_string(10),
                    get_string(10),
                    get_gender(),
                    FROM_UNIXTIME(UNIX_TIMESTAMP('1980-01-01 01:00:00') + FLOOR(RAND() * 915360000)));
            SET i = i + 1;
        END WHILE;
END$$
DELIMITER ; 
```

#### 4. Populate 40 000 000 data

```
START TRANSACTION ;
CALL generate_data(40000000);
COMMIT ;
```
1 row affected (2 hours 47 min 44.75 sec)

#### 5. Create index

```
create index my_btree_index on l8_hl.user(birth_date) using BTREE;
```

## Search tests
#### 1. for 13 790 352 rows
```
select * from l8_hl.user where birth_date between '1987-11-07 18:53:02' and '1997-11-07 18:53:02';  
```

Without BTREE index | With BTREE index
---|---
1 min 6.61 sec | 1 min 2.43 sec
explain:  
Without BTREE index  

| id | select_type | table | partitions | type | possible_keys | key  | key_len | ref  | rows     | filtered | Extra       |
|----|-------------|-------|------------|------|---------------|------|---------|------|----------|----------|-------------|
|  1 | SIMPLE      | user  | NULL       | ALL  | NULL          | NULL | NULL    | NULL | 39860184 |    11.11 | Using where |

With BTREE index  

| id | select_type | table | partitions | type | possible_keys  | key  | key_len | ref  | rows     | filtered | Extra       |
|----|-------------|-------|------------|------|----------------|------|---------|------|----------|----------|-------------|
|  1 | SIMPLE      | user  | NULL       | ALL  | my_btree_index | NULL | NULL    | NULL | 39860184 |    50.00 | Using where |

#### 2. for 1 389 441 rows
```
select * from l8_hl.user where birth_date between '1987-11-07 18:53:02' and '1988-11-07 18:53:02';
```

Without BTREE index | With BTREE index
---|---
49.99 sec | 3 min 58.00 sec

explain:  
Without BTREE index  

| id | select_type | table | partitions | type | possible_keys | key  | key_len | ref  | rows     | filtered | Extra       |
|----|-------------|-------|------------|------|---------------|------|---------|------|----------|----------|-------------|
|  1 | SIMPLE      | user  | NULL       | ALL  | NULL          | NULL | NULL    | NULL | 39860184 |    11.11 | Using where |


With BTREE index 

| id | select_type | table | partitions | type  | possible_keys  | key            | key_len | ref  | rows    | filtered | Extra                            |
|----|-------------|-------|------------|-------|----------------|----------------|---------|------|---------|----------|----------------------------------|
|  1 | SIMPLE      | user  | NULL       | range | my_btree_index | my_btree_index | 5       | NULL | 2640674 |   100.00 | Using index condition; Using MRR |


#### 3. Check search speed in the border of using index

This point around 5,2% of data.  

BTREE index: | Without BTREE index | With BTREE index
---|---|---
Between query: | '1987-11-07 18:53:02' and '1989-05-08 18:53:02' | '1987-11-07 18:53:02' and '1989-05-07 18:53:02'
Rows in result: | 2 078 065 rows | 2 074 073 rows
1-st attempt: | 56.40 sec | 6 min 50.08 sec
2-nd attempt | 57.16 sec | 5 min 3.41 sec

explain:  
Without BTREE index  

| id | select_type | table | partitions | type | possible_keys  | key  | key_len | ref  | rows     | filtered | Extra       |
|----|-------------|-------|------------|------|----------------|------|---------|------|----------|----------|-------------|
|  1 | SIMPLE      | user  | NULL       | ALL  | my_btree_index | NULL | NULL    | NULL | 39860184 |    10.23 | Using where |

With BTREE index 

| id | select_type | table | partitions | type  | possible_keys  | key            | key_len | ref  | rows    | filtered | Extra                            |
|----|-------------|-------|------------|------|----------------|------|---------|------|----------|----------|-------------|
|  1 | SIMPLE      | user  | NULL       | range | my_btree_index | my_btree_index | 5       | NULL | 3844390 |   100.00 | Using index condition; Using MRR |


## Table tests

#### 1. Check table size 

```
SELECT TABLE_NAME AS "Table",
       ROUND(((data_length + index_length) / 1024 / 1024), 2) AS "Size (MB)"
FROM information_schema.TABLES
WHERE table_schema = "l8_hl"
ORDER BY (data_length + index_length) DESC;
```

| Table | Size (MB) |
|-------|-----------|
| user  |   2260.00 |

#### 2. Remove 50% of rows
```delete from l8_hl.user where id % 2 = 0;```  
Query OK, 20000000 rows affected (46 min 37.22 sec)

#### 3. Check table size 

| Table | Size (MB) |
|-------|-----------|
| user  |   2260.00 |


#### 4. Optimize table
```OPTIMIZE TABLE user;```

| Table      | Op       | Msg_type | Msg_text                                                          |
|------------|----------|----------|-------------------------------------------------------------------|
| l8_hl.user | optimize | note     | Table does not support optimize, doing recreate + analyze instead |
| l8_hl.user | optimize | status   | OK                                                                |
2 rows in set (4 min 54.82 sec)

#### 5. Check table size 

| Table | Size (MB) |
|-------|-----------|
| user  |   2260.00 |
  
---
> _Then I decided to get up to tricky hack ..._  


#### 6. Copy data to new table 
```
CREATE TABLE l8_hl.users LIKE l8_hl.user;
INSERT INTO l8_hl.users SELECT * FROM l8_hl.user;
```
#### 7. Check table size 

| Table | Size (MB) |
|-------|-----------|
| user  |   1715.97 |
| users |   1637.98 |

> _I did it !_
